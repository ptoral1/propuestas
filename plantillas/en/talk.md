---
layout: 2021/post
section: proposals
category: talks
author: NAME
title: TALK-TITLE
---

[ABSTRACT: Short introduction and motivation of the proposal.]

## Proposal format

Indicate one of these:
-   [ ]  Short talk (10 minutes)
-   [ ]  Talk (25 minutes)

## Description

[DESCRIPTION: More extensive description of the subject and content of the proposal.]

-   Project website: [URL]

## Target audiences

[TARGET AUDIENCES: To whom?]

## Speaker/s

[SPEAKER/S: Need some general information about the person or persons who will carry out the proposal: personal interests, experience in the subject, if you have carried out similar activities...]

### Contact/s

-   Name: [NAME]
-   Email: [EMAIL]
-   Personal website: [URL]
-   Mastodon (or other free social networks): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio or GitHub (or other collaborative code sites): [URL]

## Comments

[COMMENTS: Any other comment relevant to the organization.]

## Privacy preferences

(If you want your contact information to be anonymous, you can send us the proposals using the forms on the web: <https://eslib.re/2021/proposals/talks/>)

-   [x]  I give permission for my contact email to be published with the information of the talk.
-   [x]  I give permission for my social networks to be published with the information from the talk.

## Accepted conditions

-   [x]  I agree to follow the [conduct code](https://eslib.re/conduct/) and to ask those attending to comply with it.
-   [x]  I confirm that at least one person will be online on the day scheduled to hold the talk.
