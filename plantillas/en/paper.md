---
layout: 2021/post
section: proposals
category: papers
author: NAME
title: PAPER-TITLE
---

[ABSTRACT: Short introduction and motivation of the proposal.]

## Description

[DESCRIPTION: More extensive description of the subject and content of the proposal.]

-   Project website: [URL]

## Author/s

[AUTHOR/S: In case you want to give some general information about the person or persons who authored the publication: personal interests, experience in the field, other publications made...]

### Contact/s

-   Name: [NAME]
-   Email: [EMAIL]
-   Personal website: [URL]
-   Mastodon (or other free social networks): [URL]
-   Twitter: [URL]
-   Gitlab: [URL]
-   Portfolio or GitHub (or other collaborative code sites): [URL]

## Comments

[COMMENTS: Any other comment relevant to the organization.]

## Privacy preferences

(If you want your contact information to be anonymous, you can send us the proposals using the forms on the web: <https://eslib.re/2021/proposals/papers/>)

-   [x]  I give permission for my contact email to be published with the information of the paper.
-   [x]  I give permission for my social networks to be published with the information from the paper.

## Accepted conditions

-   [x]  I agree to follow the [conduct code](https://eslib.re/conduct/) and to ask those attending to comply with it.
