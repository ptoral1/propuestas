---
layout: 2021/post
section: proposals
category: talks
author: Albert Astals Cid
title: La colección de parches KDE Qt 5.15
---

Colección de parches de KDE para Qt 5.15: <https://dot.kde.org/2021/04/06/announcing-kdes-qt-5-patch-collection>

## Formato de la propuesta

Indicar uno de estos:
-   [ ]  Charla corta (10 minutos)
-   [x]  Charla (25 minutos)

## Descripción

En abril, KDE anunció una colección de parches para Qt 5.15: <https://dot.kde.org/2021/04/06/announcing-kdes-qt-5-patch-collection>

Esta charla explicará por qué se creó esta colección de parches, qué es exactamente y cómo se mantiene.

-   Web del proyecto: <https://community.kde.org/Qt5PatchCollection>

## Público objetivo

Charla dirigida a todos aquellos interesados en entender un poco mejor cómo se gestó y como funciona esta colección de parches.

## Ponente(s)

Albert ha sido desarrollador de KDE desde 2003, desde entonces, ha trabajado en muchas áreas diferentes, desde la traducción hasta la gestión de nuevas versiones pasando por el desarrollo de visores de PDF o juegos. En 2005, Albert ganó el premio Akademy de KDE por su trabajo en KPDF (que luego se convirtió en Okular).

### Contacto(s)

-   Nombre: Albert Astals Cid
-   Email: <aacid@kde.org>
-   Web personal:
-   Mastodon (u otras redes sociales libres): <https://fosstodon.org/@tsdgeos>
-   Twitter: <https://twitter.com/tsdgeos>
-   GitLab: <https://gitlab.com/tsdgeos>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://invent.kde.org/aacid>

## Comentarios
