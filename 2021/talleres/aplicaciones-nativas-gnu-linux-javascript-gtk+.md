---
layout: 2021/post
section: proposals
category: workshops
author: Lorenzo Carbonell
title: Aplicaciones nativas en GNU/Linux con JavaScript y GTK+
---

## Descripción

Seguro que en mas de una ocasión has querido tener una sencilla aplicación para gestionar tus libros, tus colecciones, tus contactos... o simplemente para hacerte la vida algo mas sencilla. ¿Por qué no hacer esa aplicación tu mismo?

Hoy en día JavaScript es uno de los lenguajes mas utilizados, y en el caso de GNU/Linux puedes crear una aplicación nativa para el escritorio mas o menos sencilla, o tan compleja como tú quieras para hacer todo lo que tú quieras.

## Objetivos a cubrir en el taller

El objetivo del taller es crear una sencilla aplicación gráfica con la que gestionar tu colección de libros. Se trata de un ejemplo, para que se vea las posibilidades y la facilidad con la que se puede hacer.

Para llevar esto a cabo se realizará lo siguiente:

1. Conocimiento básico de JavaScript, en particular GJS
2. Revisión de los diferentes objetos con los que se puede trabajar
    1. Interfaz gráfico
    2. Gestión de datos con SQLite en GJS
3. Desarrollo de la aplicación

-   Web del proyecto: <https://www.atareao.es/tutorial/javascript-con-gjs/>

## Público objetivo

Usuarios de GNU/Linux que quieran programar su propia aplicación.

## Ponente(s)

Lorenzo Carbonell.

- Sobre mi: <https://www.atareao.es/quien-soy/>
- Actividades similares: <https://linuxcenter.es/aprende/proximos-eventos/43-bots-para-telegram-como-automatizar-hasta-el-infinito-y-mas-alla>

### Contacto(s)

-   Nombre: Lorenzo Carbonell
-   Email: <atareao@atareao.es>
-   Web personal: <https://www.atareao.es>
-   Mastodon (u otras redes sociales libres): <https://mastodon.social/@atareao>
-   Twitter: <https://twitter.com/atareao>
-   GitLab: <https://gitlab.com/atareao>
-   Portfolio o GitHub (u otros sitios de código colaborativo): <https://github.com/atareao>

## Prerrequisitos para los asistentes

- Conocimientos básicos de programación
- Hardware: un equipo con GNU/Linux
- Software: vim (o un editor de texto) y las librerías necesarias de GJS para implementar las aplicaciones

## Comentarios
